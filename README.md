# README #

This script takes a user-specified DNA sequence text file (standard FASTA) as input as well as a user-specified subsequence length. The script recursively generates all possible subsequences of that length and counts them inside the DNA sequence. The script then calculates statistics about observed and expected subsequence frequencies and prints them to standard out. Consider piping the output into a separate textfile if using a subsequence length greater than 3.

A sample invokation:

./subsequenceStats.py 3 Lemur_mitochondrial_genome_test_file.fa