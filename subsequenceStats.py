#!/usr/bin/env python
#Jens Luebeck (jensl@uw.edu)
import sys
import os

#This script takes two arguments
#arg1 nucleotide subsequence length
#arg2 FASTA sequence

if len(sys.argv) != 3:
	sys.exit("Wrong number of command line args -- specify an integer for which to determine subsequence length followed by a fasta file.")

#Recursively generates all possible DNA sequences of specified length
def buildKmers(pos,tempMer):
	global bases
	global allKmers
	global length
	if pos < length:
		for elem in bases:
			tempMer[pos] = elem
			buildKmers(pos+1,tempMer)
	else:
		allKmers.add(''.join(tempMer))

#Given the nucleotide composition of the sequence, calculates the expected frequency of observing
#a subsequence of a given length. (Assumes independence of each base)
def compFreq(subSeq):
	global freqDict
	freq = 1.0
	for base in subSeq:
		freq = freq*freqDict[base]
	return freq	

#Tests if specified DNA sequence file exists
try:
	file = open(sys.argv[2])
except IOError: 
	sys.stderr.write("Error: specified sequence file does not exist. Exiting...\n")	

#Tests if DNA subsequence length is specified as an integer 
try:
	length = int(sys.argv[1])
except ValueError:
	sys.stderr.write("Error: specified subsequence length not a valid integer. Exiting...\n")

#Parses DNA sequence from FASTA file
seq =''
for line in file:
	if not line.startswith('>'):
		seq = seq + line.rstrip()
file.close()
seq.upper()

#Generates all possible DNA subsequences of specified length
length = int(sys.argv[1])
bases = ['A','C','G','T']
allKmers = set()
tempMer = ['']*length
buildKmers(0,tempMer)


#Counts the number of times each DNA subsequence ("k-mer") exists in the DNA sequence	
counts = [0]*(length**4)
kmerCount = dict(zip(allKmers,counts))
for i in range(0,(len(seq)-1)):
	subSeq = seq[i:i+length]
	if subSeq in kmerCount:
		kmerCount[subSeq] = kmerCount[subSeq] + 1

total = len(seq)-1
sys.stdout.write("Total of " + str(total) + " subsequences of length " + str(length) + 
				" in this file." + "\n")

#Computes and prints the frequencies of each single nucleotide in the DNA sequence
freqDict = dict(zip(bases,[0.0]*len(bases)))
for elem in bases:
	totBase = seq.count(elem)
	freq = totBase/float(len(seq))
	freqDict[elem] = freq
	sys.stdout.write("Total " + elem + ": " + str(totBase) + "\t" + "Frequency " + elem + ": " 
					+ str(freq) + "\n")

#Computes and prints the frequencies of each subsequence of specified length in the DNA sequence as a table
sys.stdout.write("Subsequence" + "\t" + "Count" + "\t" + "Obsv Frequency"+ "\t" + "Exp Frequency" + "\n")
for elem in kmerCount:
	count = kmerCount[elem]
	oFreq = float(count)/float(total)
	eFreq = compFreq(elem)
	sys.stdout.write(elem + "\t" + str(count) + "\t" + str(oFreq) + "\t" + str(eFreq) + "\n")


